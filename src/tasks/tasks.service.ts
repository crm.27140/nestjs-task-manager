import { Injectable, NotFoundException } from '@nestjs/common';

import { TasksRepository } from '../database/repositories/tasks.repository';
import { Task } from '../database/entities/task.entity';

import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { UpdateTaskStatusDto } from './dto/update-task-status.dto';
import { User } from '../database/entities/user.entity';

@Injectable()
export class TasksService {
  constructor(private tasksRepository: TasksRepository) {}

  async getTasks(filters: GetTasksFilterDto, user: User): Promise<Task[]> {
    return this.tasksRepository.getTasks(filters, user);
  }

  async getTaskById(taskData: Pick<Task, 'id'>, user: User): Promise<Task> {
    const { id } = taskData;
    const task = await this.tasksRepository.findOne({
      where: {
        id,
        user: {
          id: user.id,
        },
      },
    });

    if (!task) {
      throw new NotFoundException();
    }

    return task;
  }

  async createTask(taskData: CreateTaskDto, user: User): Promise<Task> {
    return this.tasksRepository.createTask(taskData, user);
  }

  async updateTaskStatus(
    taskData: Pick<Task, 'id'> & UpdateTaskStatusDto,
    user: User,
  ): Promise<Task> {
    const taskUpdate = await this.tasksRepository.updateTaskStatus(
      taskData,
      user,
    );

    if (taskUpdate.affected === 0) {
      throw new NotFoundException();
    }

    return taskUpdate.raw[0];
  }

  async deleteTaskById(taskData: Pick<Task, 'id'>, user: User): Promise<void> {
    const { id } = taskData;

    const task = await this.tasksRepository.delete({
      id,
      user: { id: user.id },
    });

    if (task.affected === 0) {
      throw new NotFoundException();
    }
  }
}
