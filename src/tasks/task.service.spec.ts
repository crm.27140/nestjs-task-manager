import { Test } from '@nestjs/testing';
import { NotFoundException } from '@nestjs/common';

import { TasksService } from './tasks.service';
import { TasksRepository } from '../database/repositories/tasks.repository';
import { User } from '../database/entities/user.entity';

const mockTasksRepository = () => ({
  getTasks: jest.fn(),
  findOne: jest.fn(),
});

const mockUser: User = {
  username: 'test-user',
  id: 'test-user-id',
  password: 'test-password',
  tasks: [],
} as User;

describe('Task service', () => {
  let tasksService: TasksService;
  let tasksRepository;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        TasksService,
        {
          provide: TasksRepository,
          useFactory: mockTasksRepository,
        },
      ],
    }).compile();

    tasksService = module.get(TasksService);
    tasksRepository = module.get(TasksRepository);
  });

  describe('getTasks', function () {
    it('should call TasksRepository.getTasks and return the result', async () => {
      const mockedResult = 'mocked-value';
      tasksRepository.getTasks.mockResolvedValue(mockedResult);

      const result = await tasksService.getTasks(null, mockUser);
      expect(tasksRepository.getTasks).toBeCalled();
      expect(result).toEqual(mockedResult);
    });
  });

  describe('getTaskById', () => {
    it('should call TasksRepository.findOne and return the result', async () => {
      const mockedResult = {
        id: 'mocked-task',
        title: 'mocked-title',
        description: 'mocked-description',
        user: mockUser,
      };

      tasksRepository.findOne.mockResolvedValue(mockedResult);

      const result = await tasksService.getTaskById(
        { id: mockedResult.id },
        mockUser,
      );
      expect(result).toEqual(mockedResult);
      expect(tasksRepository.findOne).toBeCalled();
    });

    it('should throw the not found exception for not existing task', async () => {
      tasksRepository.findOne.mockResolvedValue(null);
      expect(
        tasksService.getTaskById({ id: 'wrong-id' }, mockUser),
      ).rejects.toThrow(NotFoundException);
    });
  });
});
