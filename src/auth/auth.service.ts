import {
  ConflictException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';

import { UserRepository } from '../database/repositories/user.repository';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { User } from '../database/entities/user.entity';
import { DatabaseErrorCode } from '../common/enums/db-errors.enum';
import { JwtPayload } from '../common/interfaces/jwt-payload.interface';

@Injectable()
export class AuthService {
  constructor(
    private userRepository: UserRepository,
    private jwtService: JwtService,
  ) {}

  async signIn(userData: AuthCredentialsDto): Promise<{ accessToken: string }> {
    const { username, password } = userData;
    const user = await this.userRepository.findOneBy({ username });

    const isPasswordCorrect = await bcrypt.compare(
      password,
      // bcrypt needs something for comparing, so we leave here an empty string if user wasn't found
      user?.password ?? '',
    );
    if (!user || !isPasswordCorrect) {
      throw new NotFoundException('Wrong username or password');
    }

    const payload: JwtPayload = {
      username: user.username,
    };

    const accessToken = this.jwtService.sign(payload);
    return { accessToken };
  }

  async signUp(userData: AuthCredentialsDto): Promise<User> {
    try {
      const user = await this.userRepository.createUser(userData);
      return user;
    } catch (error) {
      if (error.code === DatabaseErrorCode.UNIQUENESS_CONFLICT) {
        throw new ConflictException('Username already exists');
      }

      throw new InternalServerErrorException();
    }
  }
}
