import { Body, Controller, Post } from '@nestjs/common';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { AuthService } from './auth.service';
import { User } from '../database/entities/user.entity';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('signin')
  signIn(
    @Body() userData: AuthCredentialsDto,
  ): Promise<{ accessToken: string }> {
    return this.authService.signIn(userData);
  }

  @Post('signup')
  signUp(@Body() userData: AuthCredentialsDto): Promise<User> {
    return this.authService.signUp(userData);
  }
}
