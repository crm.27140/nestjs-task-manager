import {
  IsDefined,
  IsString,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';

export class AuthCredentialsDto {
  @IsString()
  @IsDefined()
  @MinLength(4, {
    message: 'Username must be at least 4 characters long',
  })
  @MaxLength(20, {
    message: 'Username must be at most 20 characters long',
  })
  username: string;

  @IsString()
  @IsDefined()
  @MinLength(8, { message: 'Password must be at least 8 characters long' })
  @MaxLength(32, {
    message: 'Password must be at least 32 characters long',
  })
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message:
      'Password must contain at least 1 upper case letter, 1 lower case letter and 1 number/special character',
  })
  password: string;
}
