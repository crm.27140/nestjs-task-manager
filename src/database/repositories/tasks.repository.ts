import { Injectable } from '@nestjs/common';
import { Repository, DataSource, UpdateResult } from 'typeorm';

import { Task } from '../entities/task.entity';
import { CreateTaskDto } from '../../tasks/dto/create-task.dto';
import { UpdateTaskStatusDto } from '../../tasks/dto/update-task-status.dto';
import { GetTasksFilterDto } from '../../tasks/dto/get-tasks-filter.dto';
import { User } from '../entities/user.entity';

@Injectable()
export class TasksRepository extends Repository<Task> {
  constructor(private dataSource: DataSource) {
    super(Task, dataSource.createEntityManager());
  }

  async getTasks(filters: GetTasksFilterDto, user: User): Promise<Task[]> {
    const { status, search } = filters;

    const tasks = await this.dataSource
      .createQueryBuilder()
      .select('task')
      .from(Task, 'task')
      .where({ user });

    if (status) {
      tasks.andWhere(`task.status = :status`, { status });
    }

    if (search) {
      tasks.andWhere(
        '(task.title LIKE :search or task.description LIKE :search)',
        {
          search: `%${search}%`,
        },
      );
    }

    return tasks.getMany();
  }

  async createTask(taskData: CreateTaskDto, user: User): Promise<Task> {
    const { title, description } = taskData;

    const task = this.create({ title, description, user });
    await task.save();

    return task;
  }

  async updateTaskStatus(
    taskData: Pick<Task, 'id'> & UpdateTaskStatusDto,
    user: User,
  ): Promise<UpdateResult> {
    const { id, status } = taskData;

    return this.dataSource
      .createQueryBuilder()
      .update(Task)
      .set({ status })
      .where('id = :id', { id })
      .andWhere({ user })
      .returning('*')
      .updateEntity(true)
      .execute();
  }
}
