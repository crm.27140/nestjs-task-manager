import * as dotenv from 'dotenv';

dotenv.config();

export const env = {
  database: {
    host: process.env['DB_HOST'],
    port: Number(process.env['DB_PORT']),
    username: process.env['DB_USERNAME'],
    password: process.env['DB_PASSWORD'],
    database: process.env['DB_NAME'],
  },
  hash: {
    secretKey: process.env['HASH_SECRET_KEY'],
  },
} as const;
